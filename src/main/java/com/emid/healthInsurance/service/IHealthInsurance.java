package com.emid.healthInsurance.service;

import org.springframework.stereotype.Service;

import com.emid.healthInsurance.bean.HealthInsuranceBean;

@Service
public interface IHealthInsurance {

	String checkPremium(HealthInsuranceBean healthInsuranceBean);

}
