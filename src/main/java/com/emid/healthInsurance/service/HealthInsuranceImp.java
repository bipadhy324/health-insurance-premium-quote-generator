package com.emid.healthInsurance.service;

import org.springframework.stereotype.Service;

import com.emid.healthInsurance.bean.CurrentHealthBean;
import com.emid.healthInsurance.bean.HabitsBean;
import com.emid.healthInsurance.bean.HealthInsuranceBean;

public class HealthInsuranceImp implements IHealthInsurance {

	@Override
	public String checkPremium(HealthInsuranceBean healthInsuranceBean) {
		double amt=0;
		String str="Health Insurance Premium for ";// Mr. Gomes: Rs. 6,856
		if(healthInsuranceBean.getAge()!=null) 
		{
			String[] age =healthInsuranceBean.getAge().split(" ");
			amt=calculateAmountBasedOnAge(age[0]);
			
		}
		if(healthInsuranceBean.getGender()!=null ) {
			if(healthInsuranceBean.getGender().equalsIgnoreCase("Males"))
			{
					str=str + " Mr. ";
					amt=amt+ amt*2/100;
				
			} else
			{
				str=str + " Mrs. ";
			}
		}
		if(healthInsuranceBean.getHabits()!=null)
		{
			amt=calculateAmountBasedOnHabit(healthInsuranceBean.getHabits(), amt);
		}
		if(healthInsuranceBean.getCurrentHealth()!=null) {
			amt=calculateAmountBasedOnCurrentHealth(healthInsuranceBean.getCurrentHealth(), amt);
		}
		
		if(healthInsuranceBean.getName()!=null) {
			String[] names=healthInsuranceBean.getName().split(" ");
			if(names.length>2) {
				str =str + names[2];
			} else if(names.length>1) {
				str =str + names[1];
			} else {
				str = str + names[0];
			}
		}
		str= str + " Rs. "+ String.valueOf(amt);
		return str;
	}

	



	private double calculateAmountBasedOnCurrentHealth(CurrentHealthBean currentHealth, double amt) {
		if(currentHealth.getBloodPressure()!=null) {
			amt=amt+amt*1/100;
		}
		if(currentHealth.getBloodSugar()!=null) {
			amt=amt+amt*1/100;
		}
		if(currentHealth.getHyperTension()!=null) {
			amt=amt+amt*1/100;
		}
		if(currentHealth.getOverWeight()!=null) {
			amt=amt+amt*1/100;
		}
		return amt;
	}





	private double calculateAmountBasedOnHabit(HabitsBean habits, double amt) {
		if(habits.getDailyExercise()!=null)
		{
			amt=amt-amt*3/100;
		} 
		if(habits.getAlcohol()!=null) {
			amt=amt+amt*3/100;
		}
		if(habits.getDrugs()!=null) {
			amt=amt+amt*3/100;
		}
		if(habits.getSmoking()!=null) {
			amt=amt+amt*3/100;
		}
		return amt;
	}



	

	private double calculateAmountBasedOnGender(String gender, double amt) {
		if(gender.equalsIgnoreCase("Males")) {
			return amt*2/100+5000;
		}
		return amt;
	}



	private double calculateAmountBasedOnAge(String age) {
		double amt=5000;
		if(age.compareTo("18")<0){
			return amt;
		} else {
			if(age.compareTo("18")>0 && age.compareTo("40")<0){
				return amt*10/100+amt;
			} else
			{
				return amt*20/100+amt;
			}
		}
		
	}

}
