package com.emid.healthInsurance.bean;

import com.fasterxml.jackson.annotation.JsonProperty;

public class HabitsBean {
	
	@JsonProperty("Smoking")
	private String smoking;
	
	@JsonProperty("Alcohol")
	private String alcohol;
	
	@JsonProperty("Daily exercise")
	private String dailyExercise;
	
	@JsonProperty("Drugs")
	private String drugs;
	
	public String getSmoking() {
		return smoking;
	}
	
	public void setSmoking(String smoking) {
		this.smoking = smoking;
	}
	
	public String getAlcohol() {
		return alcohol;
	}
	
	public void setAlcohol(String alcohol) {
		this.alcohol = alcohol;
	}
	
	public String getDailyExercise() {
		return dailyExercise;
	}
	
	public void setDailyExercise(String dailyExercise) {
		this.dailyExercise = dailyExercise;
	}
	
	public String getDrugs() {
		return drugs;
	}
	
	public void setDrugs(String drugs) {
		this.drugs = drugs;
	}
	
	@Override
	public String toString() {
		return "HabitsBean [smoking=" + smoking + ", alcohol=" + alcohol + ", dailyExercise=" + dailyExercise
				+ ", drugs=" + drugs + "]";
	}

	

}
