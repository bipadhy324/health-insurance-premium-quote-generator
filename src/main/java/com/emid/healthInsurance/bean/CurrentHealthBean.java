/**
 * 
 */
package com.emid.healthInsurance.bean;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CurrentHealthBean {

	@JsonProperty("Hypertension")
	private String hyperTension;
	
	@JsonProperty("Blood pressure")
	private String bloodPressure;
	
	@JsonProperty("Blood sugar")
	private String bloodSugar;
	
	@JsonProperty("Overweight")
	private String overWeight;

	
	public String getHyperTension() {
		return hyperTension;
	}

	
	public void setHyperTension(String hyperTension) {
		this.hyperTension = hyperTension;
	}

	
	public String getBloodPressure() {
		return bloodPressure;
	}

	
	public void setBloodPressure(String bloodPressure) {
		this.bloodPressure = bloodPressure;
	}

	
	public String getBloodSugar() {
		return bloodSugar;
	}

	
	public void setBloodSugar(String bloodSugar) {
		this.bloodSugar = bloodSugar;
	}

	
	public String getOverWeight() {
		return overWeight;
	}

	
	public void setOverWeight(String overWeight) {
		this.overWeight = overWeight;
	}

	
	@Override
	public String toString() {
		return "CurrentHealthBean [hyperTension=" + hyperTension + ", bloodPressure=" + bloodPressure + ", bloodSugar="
				+ bloodSugar + ", overWeight=" + overWeight + "]";
	}

}
