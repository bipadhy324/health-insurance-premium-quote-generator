package com.emid.healthInsurance.bean;

import com.fasterxml.jackson.annotation.JsonProperty;

public class HealthInsuranceBean {
	
	@JsonProperty("Name")
	private String name;
	
	@JsonProperty("Gender")
	private String gender;
	
	@JsonProperty("Age")
	private String age;
	
	@JsonProperty("Current health")
	private CurrentHealthBean currentHealth;
	
	@JsonProperty("Habits")
	private HabitsBean habits;

	
	public String getName() {
		return name;
	}

	
	public void setName(String name) {
		this.name = name;
	}

	
	public String getGender() {
		return gender;
	}

	
	public void setGender(String gender) {
		this.gender = gender;
	}

	
	public String getAge() {
		return age;
	}

	
	public void setAge(String age) {
		this.age = age;
	}

	
	public CurrentHealthBean getCurrentHealth() {
		return currentHealth;
	}

	
	public void setCurrentHealth(CurrentHealthBean currentHealth) {
		this.currentHealth = currentHealth;
	}

	
	public HabitsBean getHabits() {
		return habits;
	}

	
	public void setHabits(HabitsBean habits) {
		this.habits = habits;
	}

	
	@Override
	public String toString() {
		return "HealthInsuranceBean [name=" + name + ", gender=" + gender + ", age=" + age + ", currentHealth="
				+ currentHealth + ", habits=" + habits + "]";
	}

}
